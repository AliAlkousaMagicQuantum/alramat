import frappe
from frappe import _
@frappe.whitelist()

def get_customer_emails(doctype, txt, searchfield, start, page_len, filters):
	results = frappe.db.sql("""
		SELECT
			`tabCustomer Emails`.customer_email
		FROM
			`tabCustomer Emails`
		WHERE
			`tabCustomer Emails`.customer_email LIKE %(txt)s
		LIMIT
			%(start)s, %(page_len)s
	""", {
		'txt': '%' + txt + '%',
		'start': start,
		'page_len': page_len
	})

	return results