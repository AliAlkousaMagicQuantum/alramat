frappe.listview_settings['ticket Entry'] = {
    onload: function(list_view){
        let me = this;
        list_view.page.add_inner_button(("Bulk Insert Ticket Entries"), function(){
            let n = new frappe.ui.Dialog({
                fields: [
                    {
                        fieldtype: "Attach",
                        fieldname: "excel",
                        label: "Excel",
                        change: function(){
                            n.set_primary_action(("Submit"), function(values){
                                console.log('inside function ');
                                frappe.call({
                                    method: "pivottech_alramat.pivottech_alramat.doctype.ticket_entry.ticket_entry.insert_tickets",
                                    args:{
                                        filepath: values.excel
                                    },
                                    callback: function(res){
                                        
                                    }
                                }
                                
                                );
                                
                                n.hide();
                                    
                            });
                            
                            
                            
                        }
                        
                        
                    
                    }
                    
                ]
                
            });
            
            n.show();
            
        }
        
        
        )  
           
          
          
        list_view.page.add_inner_button(("Invoicing"),function(){
           
            let d = new frappe.ui.Dialog({
                title: 'Select parameters',
                fields: [
                    {
                        label: 'Start Date',
                        fieldname: 'starrt_date',
                        fieldtype: 'Date',
                        default:new Date('1/1/2023')
                    },
                    {
                        label: 'End Date',
                        fieldname: 'end_date',
                        fieldtype: 'Date',
                        default:new Date('12/31/2023')
                    },
                    {
                        label: 'Customer',
                        fieldname: 'customer',
                        fieldtype: 'Link',
                        options:'Customer',
                        
                    },
                    {
                        label: 'Supplier',
                        fieldname: 'supplier',
                        fieldtype: 'Link',
                        options:'Supplier',
                        
                    }
                   
                ],
                primary_action_label: 'Submit',
                primary_action(values) {
                    console.log(values);
                    if (values.customer)
                    {
                        frappe.call({
                            method:"pivottech_alramat.pivottech_alramat.doctype.ticket_entry.api.customer_filled",
                            args:{
                                'first_date':values.starrt_date,
                                'end_date':values.end_date,
                                'customer':values.customer,
                            },
                            callback:function(r){
                                console.log(r);
                            }
                            
                        });
                    }
                    else if (values.supplier) {
                        
                        frappe.call({
                            method:"pivottech_alramat.pivottech_alramat.doctype.ticket_entry.api.supplier_filled",
                            args:{
                                'first_date':values.starrt_date,
                                'end_date':values.end_date,
                                'supplier':values.supplier,
                            },
                            callback:function(r){
                                console.log(r);
                            }
                            
                        });



                    }
                    // get_customer_emails
                    
                    else {
                    frappe.call({
                        method:"pivottech_alramat.pivottech_alramat.doctype.ticket_entry.api.create_sales_all",
                        args:{
                            'first_date':values.starrt_date,
                            'end_date':values.end_date,
                        },
                        callback:function(r){
                            console.log(r);
                        }
                        
                    });
                    
                }
                
                    d.hide();
                }
            });
            
            d.show();
            });
       
        



        
           
    }
}
