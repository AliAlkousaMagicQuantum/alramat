import frappe 
from frappe.utils import today
from frappe.utils import getdate
import json

@frappe.whitelist()

def create_sales_all(**args):import frappe 
from frappe.utils import today
from frappe.utils import getdate
import json

@frappe.whitelist()

def create_sales_all(**args):
    
    sales_coounter=0
    purchase_counter=0
    cred=0
    PR=0
    PDiscount=0
    first_date=args.get('first_date')
    end_date=args.get('end_date')
    all_tosell_tickets=frappe.db.get_list('ticket Entry',
    fields= ['sales','conversion_rate','profit','company','currency','customer','status','commission_','net_price', 'pnr','vendor', 'eticket', 'discount', 'passenger', 'total_amount', 'name','cancelled','refund','actual_sales'],
    filters= [ ['payment_date',"between",[first_date,end_date]],],
    or_filters=[ ['status', '=', 'Saved'], ['status', '=', 'Purchase invoice created'] ] 
    
    )
    for i in all_tosell_tickets:
        total=0
        
        if not i.customer:
            pass 
        else :
            ticket_company=[]
            ticket_company=frappe.db.get_list('Company',fields=['name','employee_commission','supplier_commission','customer_commission','default_currency','default_income_account','cost_center',],
            filters= [
                        ['name','=',i.company],
                         ],)
            supplier_ticket=frappe.db.get_list('Supplier',fields=['name','default_currency',],
            filters= [
                        ['name','=',i.vendor],
                         ],)
            customer_ticket=frappe.db.get_list('Customer',fields=['name','default_currency',],
            filters= [
                        ['name','=',i.customer],
                         ],)
            if customer_ticket[0].default_currency==ticket_company[0].default_currency:
                total=i.actual_sales*i.conversion_rate
            else :
                total=abs(i.actual_sales)
                
            
            
            if i.cancelled or i.refund :
                S1=frappe.get_doc({

                "doctype":"Sales Invoice",
                "customer":i.customer,
                "company": i.company,
                "is_return":1,
                "posting_date":frappe.utils.today(),
                "due_date":frappe.utils.today(),
                "currency":customer_ticket[0].default_currency,
                
                


                "items":[
                {
                'item_code':'تذكرة',
                'item_name':'تذكر',
                'pnr':i.pnr,
                'e_ticket':i.eticket,
                'passenger':i.passenger,
                'qty':"-1",
                'uom':'تذكرة',
                'discount_amount':i.discount,
                'discount_account':ticket_company[0].customer_commission,
                'price_list_rate':abs(i.conversion_rate*i.total_amount),
                'ticketname':i.name,
                'description':'تذكرة',
                'income_account':  ticket_company[0].default_income_account,
                "emolyeeaccount":ticket_company[0].employee_commission,
                "cost_center":ticket_company[0].cost_center,
                'customer_discount':i.discount,
                "amount":abs(i.total_amount),
                #customer discount from ticket discount filed ALIALKOUSA in void in refund dont create invoice
                #


                },

                ],
                })

                S1.insert(ignore_permissions = True)
                cred=cred+1
            if not i.refund and not i.cancelled :
            

                sales_coounter=sales_coounter+1
                s=frappe.get_doc({
                        
                        "doctype":"Sales Invoice",
                        "customer":i.customer,
                        "company": i.company,
                        "posting_date":frappe.utils.today(),
                        "due_date":frappe.utils.today(),
                        "currency":customer_ticket[0].default_currency,
                        'net_profit':i.profit*i.conversion_rate,
                        
                        
                        
                        
                        

                        
                        "items":[
                            {
                                
                                'item_code':'تذكرة',
                                'item_name':'تذكر',
                                'e_ticket':i.eticket,
                                'passenger':i.passenger,
                                'qty':"1",
                                'uom':'تذكرة',
                                "pnr":i.pnr,
                                "price_list_rate":total,
                                'discount_amount':i.discount,
                                'discount_account':ticket_company[0].customer_commission,
                                'price_list_rate':abs(i.conversion_rate*i.total_amount),
                                'ticketname':i.name,
                                'description':'تذاكر',
                                'income_account':  ticket_company[0].default_income_account,
                                'customer_discount':i.discount,
                                "cost_center":ticket_company[0].cost_center,
                                "emolyeeaccount":ticket_company[0].employee_commission,
                                "amount":total,
                                 "cost_center":ticket_company[0].cost_center,
                                 
                                
                                
                                
                                
                            
                            },
                    
                        ],
                    })
                s.insert()
                
                s.save()
    frappe.msgprint(str(sales_coounter)+' Sales invoice created '+'and '+str(cred)+' credit note')
    Tickets=frappe.db.get_list('ticket Entry',
    fields= ['refund','cancelled','company','currency','customer','status','commission_','net_price', 'pnr','vendor', 'eticket', 'discount', 'passenger', 'total_amount', 'name'],
    filters= [
                    
                        
                        ['payment_date',"between",[first_date,end_date]],
                        # ['vendor','=',supplier],
                        
                       

             ],
    or_filters=[ ['status', '=', 'Saved'], ['status', '=', 'Sales invoice created'] ] 
    )
    for b in Tickets :
        # make vndor conditation 
       
        ticket_company=frappe.db.get_list('Company',fields=['supplier_commission','employee_commission','customer_commission','default_discount_account','default_currency','default_income_account','cost_center',],
            filters= [
                        ['name','=',b.company],
                         ],)
       
       
        supplier_ticket=frappe.db.get_list('Supplier',fields=['name','default_currency',],
            filters= [
                        ['name','=',b.vendor],
                         ],)
        customer_ticket=frappe.db.get_list('Customer',fields=['name','default_currency',],
            filters= [
                        ['name','=',b.customer],
                         ],)
        if  b.vendor:
            if b.cancelled or b.refund:
                if b.refund:
                    PDiscount=0
                else :
                    PDiscount=b.commission_
                PR=PR+1
                P=frappe.get_doc({
                        "doctype":"Purchase Invoice",
                        "supplier":b.vendor,
                        "company": b.company,
                        "posting_date":frappe.utils.today(),
                        "due_date":frappe.utils.today(),
                        "currency":supplier_ticket[0].default_currency,
                        "is_return":1,
                        #sup discount ticket commision  in void case and in refund is 0  ALIALKOUSA
                        
                    
                    "items":[
                            {
                                'item_code':'تذكرة',
                                'item_name':'تذكر',
                                'pnr':b.pnr,
                                'e_ticket':b.eticket,
                                'sup_discount':PDiscount,
                                'passenger':b.passenger,
                                'qty':"-1",
                                'uom':'تذكرة',
                                'discount_amount':PDiscount,
                                'discount_account':ticket_company[0].supplier_commission,
                                'price_list_rate':abs(b.total_amount),
                                'amount':abs(b.total_amount),
                                'ticketname':b.name,
                                'description':'تذكرة',
                                "reference_dn":b.name,
                                "reference_dt":'ticket Entry',
                                # 'sup_discount':0,
                                'income_account':  ticket_company[0].default_income_account,
                                "cost_center":ticket_company[0].cost_center,
                            
                            },
                    
                        ],
                    })
                
                P.insert()
                P.save()
            else :
                purchase_counter=purchase_counter+1

                p1=frappe.get_doc({
                        "doctype":"Purchase Invoice",
                        "supplier":b.vendor,
                        "company": b.company,
                        "posting_date":frappe.utils.today(),
                        "due_date":frappe.utils.today(),
                        "currency":supplier_ticket[0].default_currency,
                        
                        
                    
                    "items":[
                            {
                                'item_code':'تذكرة',
                                'item_name':'تذكر',
                                'pnr':b.pnr,
                                'e_ticket':b.eticket,
                                'sup_discount':b.commission_,
                                'passenger':b.passenger,
                                'qty':"1",
                                'uom':'تذكرة',
                                'discount_amount':0,
                                'discount_account':ticket_company[0].supplier_commission,
                                'price_list_rate':abs(b.total_amount),
                                'amount':abs(b.total_amount),
                                'ticketname':b.name,
                                'description':'تذكرة',
                                "reference_dn":b.name,
                                "reference_dt":'ticket Entry',
                                # 'sup_discount':0,
                                'income_account':  ticket_company[0].default_income_account,
                                "cost_center":ticket_company[0].cost_center,
                            
                            },
                    
                        ],
                    })
                
                p1.insert()
                p1.save()
        
    frappe.msgprint(str(purchase_counter)+' purchase invoice  created '+'and '+str(PR)+' Purchase returned') 
        # frappe.msgprint(str(sales_coounter)+' Sales invoice created '+'and '+str(cred)+' credit note')  


                
        
    return True


@frappe.whitelist()
def customer_filled(**args):
    first_date = args.get('first_date')
    end_date = args.get('end_date')
    customer_name = args.get('customer')
    
    tickets = frappe.db.get_list('ticket Entry',
        fields=[
            'conversion_rate',
            'profit',
            'company',
            'currency',
            'customer',
            'status',
            'commission_',
            'net_price',
            'pnr',
            'vendor',
            'eticket',
            'discount',
            'passenger',
            'total_amount',
            'name',
            'cancelled',
            'refund',
            'sales',
            'actual_sales'
        ],
        filters=[
            ['payment_date', 'between', [first_date, end_date]],
            ['customer', '=', customer_name]
        ],
        or_filters=[
            ['status', '=', 'Saved'],
            ['status', '=', 'Purchase invoice created']
        ]
    )

    counter = 0
    creditsn = 0
    
    for ticket in tickets:
        company = frappe.db.get_list('Company',
            fields=[
                'name',
                'supplier_commission',
                'employee_commission',
                'customer_commission',
                'default_currency',
                'default_income_account',
                'cost_center'
            ],
            filters=[['name', '=', ticket.company]],
        )[0]
        
        # supplier = frappe.db.get_list('Supplier',
        #     fields=['name', 'default_currency'],
        #     filters=[['name', '=', ticket.vendor]],
        # )[0]
        
        customer = frappe.db.get_list('Customer',
            fields=['name', 'default_currency'],
            filters=[['name', '=', ticket.customer]],
        )[0]
        
        if customer.default_currency == company.default_currency:
            total = ticket.actual_sales * ticket.conversion_rate
        else:
            total = abs(ticket.actual_sales)

        if ticket.cancelled or ticket.refund:
            creditsn += 1
            
            credit_note = frappe.get_doc({
                'doctype': 'Sales Invoice',
                'customer': ticket.customer,
                'company': ticket.company,
                'is_return': 1,
                'posting_date': frappe.utils.today(),
                'due_date': frappe.utils.today(),
                'currency': customer.default_currency,
                'items': [{
                    'item_code': 'تذكرة',
                    'item_name': 'تذكر',
                    'pnr': ticket.pnr,
                    'e_ticket': ticket.eticket,
                    'passenger': ticket.passenger,
                    'qty': '-1',
                    'uom': 'تذكرة',
                    'discount_amount': ticket.discount,
                    'price_list_rate': abs(ticket.conversion_rate*ticket.total_amount),
                    'ticketname': ticket.name,
                    'description': 'تذكرة',
                    'income_account': company.default_income_account,
                    'customer_discount': ticket.discount,
                    'amount': abs(ticket.total_amount),
                    'cost_center': company.cost_center,
                    'emolyeeaccount': company.employee_commission,
                    'discount_account': company.customer_commission,
                    
                }]
            })
            
            credit_note.insert(ignore_permissions=True)
            credit_note.save()

        if not ticket.cancelled and not ticket.refund:
            counter += 1
            
            sales_invoice = frappe.get_doc({
                'doctype': 'Sales Invoice',
                'customer': ticket.customer,
                'company': ticket.company,
                'posting_date': frappe.utils.today(),
                'due_date': frappe.utils.today(),
                'currency': customer.default_currency,
                'net_profit':ticket.profit*ticket.conversion_rate,
                'items': [{
                    'item_code': 'تذكرة',
                    'item_name': 'تذكر',
                    'e_ticket': ticket.eticket,
                    'passenger': ticket.passenger,
                    'qty': '1',
                    'uom': 'تذكرة',
                    'pnr': ticket.pnr,
                    'discount_amount': ticket.discount,
                    'price_list_rate': abs(ticket.conversion_rate*ticket.total_amount),
                    'ticketname': ticket.name,
                    'description': 'تذكرة',
                    'income_account': company.default_income_account,
                    'customer_discount': ticket.discount,
                    'amount': total,
                    'cost_center': company.cost_center,
                    'emolyeeaccount': company.employee_commission,
                    'discount_account': company.customer_commission,
                    
                }]
            })
            
            sales_invoice.insert(ignore_permissions=True)
            sales_invoice.save()

    frappe.msgprint(str(counter) + ' sales invoices created for customer ' + str(customer_name) + ' and ' + str(creditsn) + ' credit notes.')
    
    return True
     
    
@frappe.whitelist()
def supplier_filled(**args):
    first_date = args.get('first_date')
    end_date = args.get('end_date')
    supplier = args.get('supplier')
    
    tickets = frappe.db.get_list('ticket Entry',
        fields=[
            'cancelled',
            'refund',
            'company',
            'currency',
            'customer',
            'status',
            'commission_',
            'net_price',
            'pnr',
            'vendor',
            'eticket',
            'discount',
            'passenger',
            'total_amount',
            'name'
        ],
        filters=[
            ['payment_date', 'between', [first_date, end_date]],
            ['vendor', '=', supplier]
        ],
        or_filters=[
            ['status', '=', 'Saved'],
            ['status', '=', 'Sales invoice created']
        ]
    )
    
    counter = 0
    
    for ticket in tickets:
        company = frappe.db.get_list('Company',
            fields=[
                'supplier_commission',
                'employee_commission',
                'customer_commission',
                'default_discount_account',
                'default_currency',
                'default_income_account',
                'cost_center'
            ],
            filters=[['name', '=', ticket.company]],
        )[0]
        
        supplier = frappe.db.get_list('Supplier',
            fields=['name', 'default_currency'],
            filters=[['name', '=', ticket.vendor]],
        )[0]
        
        if ticket.cancelled or ticket.refund:
            discount = 0 if ticket.refund else ticket.commission_
            
            purchase_invoice = frappe.get_doc({
                'doctype': 'Purchase Invoice',
                'supplier': ticket.vendor,
                'company': ticket.company,
                'posting_date': frappe.utils.today(),
                'due_date': getdate('2023-11-19'),
                'currency': supplier.default_currency,
                'is_return': 1,
                'items': [{
                    'item_code': 'تذكرة',
                    'item_name': 'تذكرة',
                    'pnr': ticket.pnr,
                    'e_ticket': ticket.eticket,
                    'sup_discount': discount,
                    'passenger': ticket.passenger,
                    'qty': '-1',
                    'uom': 'تذكرة',
                    'discount_amount': discount,
                    'discount_account': company.supplier_commission,
                    'price_list_rate': abs(ticket.total_amount),
                    'amount': abs(ticket.total_amount),
                    'ticketname': ticket.name,
                    'description': 'تذكرة',
                    'cost_center': company.cost_center,
                }],
            })
            
            purchase_invoice.insert()
            purchase_invoice.save()
        else:
            counter += 1
            
            purchase_invoice = frappe.get_doc({
                'doctype': 'Purchase Invoice',
                'supplier': ticket.vendor,
                'company': ticket.company,
                'posting_date': frappe.utils.today(),
                'due_date': getdate('2023-11-19'),
                'currency': supplier.default_currency,
                'items': [{
                    'item_code': 'تذكرة',
                    'item_name': 'تذكرة',
                    'pnr': ticket.pnr,
                    'e_ticket': ticket.eticket,
                    'sup_discount': ticket.commission_,
                    'passenger': ticket.passenger,
                    'qty': '1',
                    'uom': 'تذكرة',
                    'discount_amount': 0,
                    'discount_account': company.supplier_commission,
                    'price_list_rate': abs(ticket.total_amount),
                    'amount': abs(ticket.total_amount),
                    'ticketname': ticket.name,
                    'description': 'تذكرة',
                    'cost_center': company.cost_center,
                }],
            })
            
            purchase_invoice.insert()
            purchase_invoice.save()
    
    frappe.msgprint(f'{counter} purchase invoices created')
    return True
@frappe.whitelist()
def get_customer_emails(doctype, txt, searchfield, start, page_len, filters):
    sql_query = """
        SELECT email_id FROM `tabContacts Emails Unique`
        
    """

    email_ids = frappe.db.sql(sql_query, as_dict=True)
    return [e["email_id"] for e in email_ids]

@frappe.whitelist()
def get_customers_by_email(email_name, start=0, page_len=20):

    results = frappe.db.sql("""
        SELECT
            `tabContacts Emails Unique`.parent
        FROM
            `tabContacts Emails Unique`
        WHERE
            `tabContacts Emails Unique`.name LIKE %(email_name)s
        LIMIT
            %(start)s, %(page_len)s
    """, {
        'email_name': '%' + email_name + '%',
        'start': start,
        'page_len': page_len
    }, as_dict=True)
    customer_names = set()

    for contact_name in results:
        contact_name=contact_name.get("parent")
        contact = frappe.get_doc('Contact', contact_name)

        links=contact.links
        for link in links:
                    if link.link_doctype == 'Customer' and link.link_name not in customer_names:
                        customer_names.add(link.link_name)
    customers = frappe.get_all('Customer', filters={'name': ['in', list(customer_names)]}, fields=['name', 'customer_name'])
    return customer_names

    # result contain all contact names 
    # then foreach name we should get all customer in links 


# @frappe.whitelist()
# def get_contacts_with_customer(customer_name):
#     emailss=[]
#     customer = frappe.get_doc("Customer", customer_name)
#     customer_email_list = []
#     contacts = frappe.db.sql("""
#         SELECT DISTINCT `tabContact`.`name`
#         FROM `tabContact`
#         INNER JOIN (
#             SELECT `parent`
#             FROM `tabDynamic Link`
#             WHERE `link_doctype` = 'Customer' AND `link_name` = %s
#         ) AS `cl` ON `tabContact`.`name` = `cl`.`parent`
#     """, (customer_name,)
#     )
#     for contact_name in contacts:
#         contact_emails = frappe.db.sql("""
#             SELECT DISTINCT `email_id`
#             FROM `tabContact unique Email`
#             WHERE `parent` = %(contact_id)s
#         """, {'contact_id': contact_name[0]})

#         # Add each email address to the customer_email list
#         for email in contact_emails:
#             customer_emails = customer.append('customer_emails', {})
#             customer_emails.customer_email = email[0]
#             customer_email_list.append(customer_emails)

#     # Save the customer document
#     customer.save()

   
#     return True
@frappe.whitelist()
def get_contacts_with_customer(customer_name):
    customer = frappe.get_doc("Customer", customer_name)
    customer_email_list = []
    contacts = frappe.db.sql("""
        SELECT DISTINCT `tabContact`.`name`
        FROM `tabContact`
        INNER JOIN (
            SELECT `parent`
            FROM `tabDynamic Link`
            WHERE `link_doctype` = 'Customer' AND `link_name` = %s
        ) AS `cl` ON `tabContact`.`name` = `cl`.`parent`
    """, (customer_name,)
    )
    for contact_name in contacts:
        contact_emails = frappe.db.sql("""
            SELECT DISTINCT `email_id`
            FROM `tabContacts Emails Unique`
            WHERE `parent` = %(contact_id)s
        """, {'contact_id': contact_name[0]})

        # Add each email address to the customer_email list if it doesn't already exist
        for email in contact_emails:
            if email[0] not in [e.customer_email for e in customer.customer_emails]:
                customer_emails = customer.append('customer_emails', {})
                customer_emails.customer_email = email[0]
                customer_email_list.append(customer_emails)

    # Save the customer document
    customer.save()

    return True
@frappe.whitelist()
def create_notification_log(self, user, message ):
    notification_log = frappe.get_doc({
        'doctype': 'Notification Log',
        'user': user,
        'message': message,
        
        
    })
    notification_log.insert()

    return "Notification log created successfully"
    
    sales_coounter=0
    purchase_counter=0
    cred=0
    PR=0
    PDiscount=0
    first_date=args.get('first_date')
    end_date=args.get('end_date')
    all_tosell_tickets=frappe.db.get_list('ticket Entry',
    fields= ['sales','conversion_rate','profit','company','currency','customer','status','commission_','net_price', 'pnr','vendor', 'eticket', 'discount', 'passenger', 'total_amount', 'name','cancelled','refund'],
    filters= [ ['payment_date',"between",[first_date,end_date]],],
    or_filters=[ ['status', '=', 'Saved'], ['status', '=', 'Purchase invoice created'] ] 
    
    )
    for i in all_tosell_tickets:
        total=0
        
        if not i.customer:
            pass 
        else :
            ticket_company=[]
            ticket_company=frappe.db.get_list('Company',fields=['name','employee_commission','supplier_commission','customer_commission','default_currency','default_income_account','cost_center',],
            filters= [
                        ['name','=',i.company],
                         ],)
            supplier_ticket=frappe.db.get_list('Supplier',fields=['name','default_currency',],
            filters= [
                        ['name','=',i.vendor],
                         ],)
            customer_ticket=frappe.db.get_list('Customer',fields=['name','default_currency',],
            filters= [
                        ['name','=',i.customer],
                         ],)
            if customer_ticket[0].default_currency==ticket_company[0].default_currency:
                total=i.sales*i.conversion_rate
            else :
                total=abs(i.sales)
                
            
            
            if i.cancelled or i.refund :
                S1=frappe.get_doc({

                "doctype":"Sales Invoice",
                "customer":i.customer,
                "company": i.company,
                "is_return":1,
                "posting_date":frappe.utils.today(),
                "due_date":frappe.utils.today(),
                "currency":customer_ticket[0].default_currency,
                
                


                "items":[
                {
                'item_code':'تذكرة',
                'item_name':'تذكر',
                'pnr':i.pnr,
                'e_ticket':i.eticket,
                'passenger':i.passenger,
                'qty':"-1",
                'uom':'تذكرة',
                'discount_amount':i.discount,
                'discount_account':ticket_company[0].customer_commission,
                'price_list_rate':abs(i.conversion_rate*i.total_amount),
                'ticketname':i.name,
                'description':'تذكرة',
                'income_account':  ticket_company[0].default_income_account,
                "emolyeeaccount":ticket_company[0].employee_commission,
                "cost_center":ticket_company[0].cost_center,
                'customer_discount':i.discount,
                "amount":abs(i.total_amount),
                #customer discount from ticket discount filed ALIALKOUSA in void in refund dont create invoice
                #


                },

                ],
                })

                S1.insert(ignore_permissions = True)
                cred=cred+1
            if not i.refund and not i.cancelled :
            

                sales_coounter=sales_coounter+1
                s=frappe.get_doc({
                        
                        "doctype":"Sales Invoice",
                        "customer":i.customer,
                        "company": i.company,
                        "posting_date":frappe.utils.today(),
                        "due_date":frappe.utils.today(),
                        "currency":customer_ticket[0].default_currency,
                        'net_profit':i.profit*i.conversion_rate,
                        
                        
                        
                        
                        

                        
                        "items":[
                            {
                                
                                'item_code':'تذكرة',
                                'item_name':'تذكر',
                                'e_ticket':i.eticket,
                                'passenger':i.passenger,
                                'qty':"1",
                                'uom':'تذكرة',
                                "pnr":i.pnr,
                                "price_list_rate":total,
                                'discount_amount':i.discount,
                                'discount_account':ticket_company[0].customer_commission,
                                'price_list_rate':abs(i.conversion_rate*i.total_amount),
                                'ticketname':i.name,
                                'description':'تذاكر',
                                'income_account':  ticket_company[0].default_income_account,
                                'customer_discount':i.discount,
                                "cost_center":ticket_company[0].cost_center,
                                "emolyeeaccount":ticket_company[0].employee_commission,
                                "amount":total,
                                 "cost_center":ticket_company[0].cost_center,
                                 
                                
                                
                                
                                
                            
                            },
                    
                        ],
                    })
                s.insert()
                
                s.save()
    frappe.msgprint(str(sales_coounter)+' Sales invoice created '+'and '+str(cred)+' credit note')
    Tickets=frappe.db.get_list('ticket Entry',
    fields= ['refund','cancelled','company','currency','customer','status','commission_','net_price', 'pnr','vendor', 'eticket', 'discount', 'passenger', 'total_amount', 'name'],
    filters= [
                    
                        
                        ['payment_date',"between",[first_date,end_date]],
                        # ['vendor','=',supplier],
                        
                       

             ],
    or_filters=[ ['status', '=', 'Saved'], ['status', '=', 'Sales invoice created'] ] 
    )
    for b in Tickets :
        # make vndor conditation 
       
        ticket_company=frappe.db.get_list('Company',fields=['supplier_commission','employee_commission','customer_commission','default_discount_account','default_currency','default_income_account','cost_center',],
            filters= [
                        ['name','=',b.company],
                         ],)
       
       
        supplier_ticket=frappe.db.get_list('Supplier',fields=['name','default_currency',],
            filters= [
                        ['name','=',b.vendor],
                         ],)
        customer_ticket=frappe.db.get_list('Customer',fields=['name','default_currency',],
            filters= [
                        ['name','=',b.customer],
                         ],)
        if  b.vendor:
            if b.cancelled or b.refund:
                if b.refund:
                    PDiscount=0
                else :
                    PDiscount=b.commission_
                PR=PR+1
                P=frappe.get_doc({
                        "doctype":"Purchase Invoice",
                        "supplier":b.vendor,
                        "company": b.company,
                        "posting_date":frappe.utils.today(),
                        "due_date":frappe.utils.today(),
                        "currency":supplier_ticket[0].default_currency,
                        "is_return":1,
                        #sup discount ticket commision  in void case and in refund is 0  ALIALKOUSA
                        
                    
                    "items":[
                            {
                                'item_code':'تذكرة',
                                'item_name':'تذكر',
                                'pnr':b.pnr,
                                'e_ticket':b.eticket,
                                'sup_discount':PDiscount,
                                'passenger':b.passenger,
                                'qty':"-1",
                                'uom':'تذكرة',
                                'discount_amount':PDiscount,
                                'discount_account':ticket_company[0].supplier_commission,
                                'price_list_rate':abs(b.total_amount),
                                'amount':abs(b.total_amount),
                                'ticketname':b.name,
                                'description':'تذكرة',
                                "reference_dn":b.name,
                                "reference_dt":'ticket Entry',
                                # 'sup_discount':0,
                                'income_account':  ticket_company[0].default_income_account,
                                "cost_center":ticket_company[0].cost_center,
                            
                            },
                    
                        ],
                    })
                
                P.insert()
                P.save()
            else :
                purchase_counter=purchase_counter+1

                p1=frappe.get_doc({
                        "doctype":"Purchase Invoice",
                        "supplier":b.vendor,
                        "company": b.company,
                        "posting_date":frappe.utils.today(),
                        "due_date":frappe.utils.today(),
                        "currency":supplier_ticket[0].default_currency,
                        
                        
                    
                    "items":[
                            {
                                'item_code':'تذكرة',
                                'item_name':'تذكر',
                                'pnr':b.pnr,
                                'e_ticket':b.eticket,
                                'sup_discount':b.commission_,
                                'passenger':b.passenger,
                                'qty':"1",
                                'uom':'تذكرة',
                                'discount_amount':0,
                                'discount_account':ticket_company[0].supplier_commission,
                                'price_list_rate':abs(b.total_amount),
                                'amount':abs(b.total_amount),
                                'ticketname':b.name,
                                'description':'تذكرة',
                                "reference_dn":b.name,
                                "reference_dt":'ticket Entry',
                                # 'sup_discount':0,
                                'income_account':  ticket_company[0].default_income_account,
                                "cost_center":ticket_company[0].cost_center,
                            
                            },
                    
                        ],
                    })
                
                p1.insert()
                p1.save()
        
    frappe.msgprint(str(purchase_counter)+' purchase invoice  created '+'and '+str(PR)+' Purchase returned') 
        # frappe.msgprint(str(sales_coounter)+' Sales invoice created '+'and '+str(cred)+' credit note')  


                
        
    return True


@frappe.whitelist()
def customer_filled(**args):
    first_date = args.get('first_date')
    end_date = args.get('end_date')
    customer_name = args.get('customer')
    
    tickets = frappe.db.get_list('ticket Entry',
        fields=[
            'conversion_rate',
            'profit',
            'company',
            'currency',
            'customer',
            'status',
            'commission_',
            'net_price',
            'pnr',
            'vendor',
            'eticket',
            'discount',
            'passenger',
            'total_amount',
            'name',
            'cancelled',
            'refund',
            'sales'
        ],
        filters=[
            ['payment_date', 'between', [first_date, end_date]],
            ['customer', '=', customer_name]
        ],
        or_filters=[
            ['status', '=', 'Saved'],
            ['status', '=', 'Purchase invoice created']
        ]
    )

    counter = 0
    creditsn = 0
    
    for ticket in tickets:
        company = frappe.db.get_list('Company',
            fields=[
                'name',
                'supplier_commission',
                'employee_commission',
                'customer_commission',
                'default_currency',
                'default_income_account',
                'cost_center'
            ],
            filters=[['name', '=', ticket.company]],
        )[0]
        
        # supplier = frappe.db.get_list('Supplier',
        #     fields=['name', 'default_currency'],
        #     filters=[['name', '=', ticket.vendor]],
        # )[0]
        
        customer = frappe.db.get_list('Customer',
            fields=['name', 'default_currency'],
            filters=[['name', '=', ticket.customer]],
        )[0]
        
        if customer.default_currency == company.default_currency:
            total = ticket.sales * ticket.conversion_rate
        else:
            total = abs(ticket.sales)

        if ticket.cancelled or ticket.refund:
            creditsn += 1
            
            credit_note = frappe.get_doc({
                'doctype': 'Sales Invoice',
                'customer': ticket.customer,
                'company': ticket.company,
                'is_return': 1,
                'posting_date': frappe.utils.today(),
                'due_date': frappe.utils.today(),
                'currency': customer.default_currency,
                'items': [{
                    'item_code': 'تذكرة',
                    'item_name': 'تذكر',
                    'pnr': ticket.pnr,
                    'e_ticket': ticket.eticket,
                    'passenger': ticket.passenger,
                    'qty': '-1',
                    'uom': 'تذكرة',
                    'discount_amount': ticket.discount,
                    'price_list_rate': abs(ticket.conversion_rate*ticket.total_amount),
                    'ticketname': ticket.name,
                    'description': 'تذكرة',
                    'income_account': company.default_income_account,
                    'customer_discount': ticket.discount,
                    'amount': abs(ticket.total_amount),
                    'cost_center': company.cost_center,
                    'emolyeeaccount': company.employee_commission,
                    'discount_account': company.customer_commission,
                    
                }]
            })
            
            credit_note.insert(ignore_permissions=True)
            credit_note.save()

        if not ticket.cancelled and not ticket.refund:
            counter += 1
            
            sales_invoice = frappe.get_doc({
                'doctype': 'Sales Invoice',
                'customer': ticket.customer,
                'company': ticket.company,
                'posting_date': frappe.utils.today(),
                'due_date': frappe.utils.today(),
                'currency': customer.default_currency,
                'net_profit':ticket.profit*ticket.conversion_rate,
                'items': [{
                    'item_code': 'تذكرة',
                    'item_name': 'تذكر',
                    'e_ticket': ticket.eticket,
                    'passenger': ticket.passenger,
                    'qty': '1',
                    'uom': 'تذكرة',
                    'pnr': ticket.pnr,
                    'discount_amount': ticket.discount,
                    'price_list_rate': abs(ticket.conversion_rate*ticket.total_amount),
                    'ticketname': ticket.name,
                    'description': 'تذكرة',
                    'income_account': company.default_income_account,
                    'customer_discount': ticket.discount,
                    'amount': total,
                    'cost_center': company.cost_center,
                    'emolyeeaccount': company.employee_commission,
                    'discount_account': company.customer_commission,
                    
                }]
            })
            
            sales_invoice.insert(ignore_permissions=True)
            sales_invoice.save()

    frappe.msgprint(str(counter) + ' sales invoices created for customer ' + str(customer_name) + ' and ' + str(creditsn) + ' credit notes.')
    
    return True
     
    
@frappe.whitelist()
def supplier_filled(**args):
    first_date = args.get('first_date')
    end_date = args.get('end_date')
    supplier = args.get('supplier')
    
    tickets = frappe.db.get_list('ticket Entry',
        fields=[
            'cancelled',
            'refund',
            'company',
            'currency',
            'customer',
            'status',
            'commission_',
            'net_price',
            'pnr',
            'vendor',
            'eticket',
            'discount',
            'passenger',
            'total_amount',
            'name'
        ],
        filters=[
            ['payment_date', 'between', [first_date, end_date]],
            ['vendor', '=', supplier]
        ],
        or_filters=[
            ['status', '=', 'Saved'],
            ['status', '=', 'Sales invoice created']
        ]
    )
    
    counter = 0
    
    for ticket in tickets:
        company = frappe.db.get_list('Company',
            fields=[
                'supplier_commission',
                'employee_commission',
                'customer_commission',
                'default_discount_account',
                'default_currency',
                'default_income_account',
                'cost_center'
            ],
            filters=[['name', '=', ticket.company]],
        )[0]
        
        supplier = frappe.db.get_list('Supplier',
            fields=['name', 'default_currency'],
            filters=[['name', '=', ticket.vendor]],
        )[0]
        
        if ticket.cancelled or ticket.refund:
            discount = 0 if ticket.refund else ticket.commission_
            
            purchase_invoice = frappe.get_doc({
                'doctype': 'Purchase Invoice',
                'supplier': ticket.vendor,
                'company': ticket.company,
                'posting_date': frappe.utils.today(),
                'due_date': getdate('2023-11-19'),
                'currency': supplier.default_currency,
                'is_return': 1,
                'items': [{
                    'item_code': 'تذكرة',
                    'item_name': 'تذكرة',
                    'pnr': ticket.pnr,
                    'e_ticket': ticket.eticket,
                    'sup_discount': discount,
                    'passenger': ticket.passenger,
                    'qty': '-1',
                    'uom': 'تذكرة',
                    'discount_amount': discount,
                    'discount_account': company.supplier_commission,
                    'price_list_rate': abs(ticket.total_amount),
                    'amount': abs(ticket.total_amount),
                    'ticketname': ticket.name,
                    'description': 'تذكرة',
                    'cost_center': company.cost_center,
                }],
            })
            
            purchase_invoice.insert()
            purchase_invoice.save()
        else:
            counter += 1
            
            purchase_invoice = frappe.get_doc({
                'doctype': 'Purchase Invoice',
                'supplier': ticket.vendor,
                'company': ticket.company,
                'posting_date': frappe.utils.today(),
                'due_date': getdate('2023-11-19'),
                'currency': supplier.default_currency,
                'items': [{
                    'item_code': 'تذكرة',
                    'item_name': 'تذكرة',
                    'pnr': ticket.pnr,
                    'e_ticket': ticket.eticket,
                    'sup_discount': ticket.commission_,
                    'passenger': ticket.passenger,
                    'qty': '1',
                    'uom': 'تذكرة',
                    'discount_amount': 0,
                    'discount_account': company.supplier_commission,
                    'price_list_rate': abs(ticket.total_amount),
                    'amount': abs(ticket.total_amount),
                    'ticketname': ticket.name,
                    'description': 'تذكرة',
                    'cost_center': company.cost_center,
                }],
            })
            
            purchase_invoice.insert()
            purchase_invoice.save()
    
    frappe.msgprint(f'{counter} purchase invoices created')
    return True
@frappe.whitelist()
def get_customer_emails(doctype, txt, searchfield, start, page_len, filters):
    sql_query = """
        SELECT email_id FROM `tabContacts Emails Unique`
        
    """

    email_ids = frappe.db.sql(sql_query, as_dict=True)
    return [e["email_id"] for e in email_ids]

@frappe.whitelist()
def get_customers_by_email(email_name, start=0, page_len=20):

    results = frappe.db.sql("""
        SELECT
            `tabContacts Emails Unique`.parent
        FROM
            `tabContacts Emails Unique`
        WHERE
            `tabContacts Emails Unique`.name LIKE %(email_name)s
        LIMIT
            %(start)s, %(page_len)s
    """, {
        'email_name': '%' + email_name + '%',
        'start': start,
        'page_len': page_len
    }, as_dict=True)
    customer_names = set()

    for contact_name in results:
        contact_name=contact_name.get("parent")
        contact = frappe.get_doc('Contact', contact_name)

        links=contact.links
        for link in links:
                    if link.link_doctype == 'Customer' and link.link_name not in customer_names:
                        customer_names.add(link.link_name)
    customers = frappe.get_all('Customer', filters={'name': ['in', list(customer_names)]}, fields=['name', 'customer_name'])
    return customer_names

    # result contain all contact names 
    # then foreach name we should get all customer in links 


# @frappe.whitelist()
# def get_contacts_with_customer(customer_name):
#     emailss=[]
#     customer = frappe.get_doc("Customer", customer_name)
#     customer_email_list = []
#     contacts = frappe.db.sql("""
#         SELECT DISTINCT `tabContact`.`name`
#         FROM `tabContact`
#         INNER JOIN (
#             SELECT `parent`
#             FROM `tabDynamic Link`
#             WHERE `link_doctype` = 'Customer' AND `link_name` = %s
#         ) AS `cl` ON `tabContact`.`name` = `cl`.`parent`
#     """, (customer_name,)
#     )
#     for contact_name in contacts:
#         contact_emails = frappe.db.sql("""
#             SELECT DISTINCT `email_id`
#             FROM `tabContact unique Email`
#             WHERE `parent` = %(contact_id)s
#         """, {'contact_id': contact_name[0]})

#         # Add each email address to the customer_email list
#         for email in contact_emails:
#             customer_emails = customer.append('customer_emails', {})
#             customer_emails.customer_email = email[0]
#             customer_email_list.append(customer_emails)

#     # Save the customer document
#     customer.save()

   
#     return True
@frappe.whitelist()
def get_contacts_with_customer(customer_name):
    customer = frappe.get_doc("Customer", customer_name)
    customer_email_list = []
    contacts = frappe.db.sql("""
        SELECT DISTINCT `tabContact`.`name`
        FROM `tabContact`
        INNER JOIN (
            SELECT `parent`
            FROM `tabDynamic Link`
            WHERE `link_doctype` = 'Customer' AND `link_name` = %s
        ) AS `cl` ON `tabContact`.`name` = `cl`.`parent`
    """, (customer_name,)
    )
    for contact_name in contacts:
        contact_emails = frappe.db.sql("""
            SELECT DISTINCT `email_id`
            FROM `tabContacts Emails Unique`
            WHERE `parent` = %(contact_id)s
        """, {'contact_id': contact_name[0]})

        # Add each email address to the customer_email list if it doesn't already exist
        for email in contact_emails:
            if email[0] not in [e.customer_email for e in customer.customer_emails]:
                customer_emails = customer.append('customer_emails', {})
                customer_emails.customer_email = email[0]
                customer_email_list.append(customer_emails)

    # Save the customer document
    customer.save()

    return True
@frappe.whitelist()
def create_notification_log(self, user, message ):
    notification_log = frappe.get_doc({
        'doctype': 'Notification Log',
        'user': user,
        'message': message,
        
        
    })
    notification_log.insert()

    return "Notification log created successfully"